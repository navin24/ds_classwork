#include<stdio.h>

int fact_rec(int num);

int main(void)
{
    int num;
    printf("Enter a number  : ");
    scanf("%d",&num);
    if(num==0)
        printf("Factorial of %d :   1\n",num);
    else 
        printf("Factorial of %d :   %d\n",num,fact_rec(num));


    return 0;
}

int fact_rec(int num)
{
    if(num==1)
        return 1;
    else
        return(num * fact_rec(num-1));
}