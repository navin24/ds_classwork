#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    int data;
    struct node *next;
}node_t;

typedef struct{
    node_t *head;
    int cnt;
}list_t;

void init_list(list_t *list);
void add_last(list_t *list);
node_t* create_newnode(int data);
int main(void)
{
    list_t list;
    init_list(&list);

    add_last(&list);
}

void init_list(list_t *list)
{
    list->head = NULL;
    list->cnt = 0;
}
node_t* create_newnode(int data)
{
    node_t *temp = (node_t*)malloc(sizeof(node_t));
    temp->data = data;
    temp->next = NULL;

    return temp;
    
}
void add_last(list_t *list)
{
    int data;
    printf("Enter Data  :   ");
    scanf("%d",&data);
    node_t *newnode = create_newnode(data);
}