#include<stdio.h>

int power_rec(int base, int pow);

int main(void)
{
    int base,pow;
    printf("Enter base  : ");
    scanf("%d",&base);
    printf("Enter power  : ");
    scanf("%d",&pow);
    if(base==0 || base ==1)
        printf("Base of %d to any power :   1\n");
    else 
        printf("Value of base(%d) to power(%d)  :   %d\n",base,pow,power_rec(base,pow));
    return 0;
}

int power_rec(int base , int pow)
{
    if(pow!=0)
        return(base * power_rec(base,pow-1));
    else
        return 1;
}