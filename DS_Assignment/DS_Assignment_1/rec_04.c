#include<stdio.h>

int bin_rec(int num1);

int main(void)
{
    int num1,num2;
    printf("Enter num1      :   ");
    scanf("%d",&num1);
    printf("Binary value of %d    :   %d\n",num1,bin_rec(num1));
    return 0;
}

int bin_rec(int num1)
{
    
    if(num1==0)
        return 0;
    else
        return(num1%2+10*bin_rec(num1/2));
}