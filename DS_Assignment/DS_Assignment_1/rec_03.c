#include<stdio.h>

int gcd_rec(int num1,int num2);

int main(void)
{
    int num1,num2;
    printf("Enter num1      :   ");
    scanf("%d",&num1);
    printf("Enter num2      :   ");
    scanf("%d",&num2);
    printf("GCD of %d and %d    :   %d\n",num1,num2,gcd_rec(num1,num2));
    return 0;
}

int gcd_rec(int num1,int num2)
{
    while(num1!=num2)
    {
        if(num1<num2)
        {
            return gcd_rec(num1,num2-num1);
        }
        else
        {
            return gcd_rec(num1-num2,num2);
        }
    }
    return num1;
}