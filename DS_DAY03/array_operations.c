/* Program to perform array operations */
#include<stdio.h>

/* global declaration section */
#define SWAP(a,b) { int t = a; a=b; b=t; }

#define SIZE 8
#define true 1
#define false 0

typedef int bool_t;

int comparisons = 0;

//function declarations	
void display_array_elements(int arr[]);
int array_sum(int arr[]);
int rec_array_sum(int arr[], int index);
bool_t linear_search(int arr[], int key);
bool_t rec_linear_search(int arr[], int key, int index);
bool_t updated_linear_search(int arr[], int key);
bool_t binary_search(int arr[], int key);
bool_t rec_binary_search(int arr[], int key, int left, int right);
void selection_sort(int arr[]);


//entry point function
int main(void)
{
	//int arr[SIZE] = {10,20,30,40,50,60,70,80,90,100};
	int arr[SIZE] = {30,20,80,60,50,70,10,40};
	int key;

	//display_array_elements(arr);
	//printf("sum of array ele's is: %d\n", array_sum(arr));
	//printf("sum of array ele's is: %d\n", rec_array_sum(arr,0));//initialization
	display_array_elements(arr);
	selection_sort(arr);
	display_array_elements(arr);

	/*
	printf("enter the key: ");
	scanf("%d", &key);

	//if( linear_search(arr, key) )
	//if( rec_linear_search(arr,key,0) )
	//if( updated_linear_search(arr,key) )
	//if( binary_search(arr, key) )
	if( rec_binary_search(arr, key, 0, SIZE-1) )
	{
		printf("%d is found in an array, no. of comparisons = %d\n", key, comparisons);
	}
	else
	{
		printf("%d is not found in an array, no. of comparisons = %d\n", key, comparisons);
	}
	*/


	return 0;
}//end of main() function

//function definitions
void selection_sort(int arr[])
{
	int sel_pos;
	int pos;
	int iterations=0;
	int comparisons=0;

	for( sel_pos = 0 ; sel_pos < SIZE-1 ; sel_pos++ )
	{
		iterations++;
		for( pos = sel_pos+1 ; pos < SIZE ; pos++ )
		{
			comparisons++;
			if( arr[ sel_pos ] > arr[ pos ] )
				SWAP(arr[ sel_pos ], arr[ pos ] );
		}
	}

	printf("no. of iterations are: %d\n", iterations);
	printf("no. of comprisons are: %d\n", comparisons);
}


bool_t rec_binary_search(int arr[], int key, int left, int right)
{
	//base condition
	if( left > right )
		return false;
	
	int mid = (left+right)/2;

	comparisons++;
	if( key == arr[ mid ] )
		return true;

	if( key < arr[ mid ] )
		return rec_binary_search(arr,key,left,mid-1);
	else
		return rec_binary_search(arr,key,mid+1,right);
}

bool_t binary_search(int arr[], int key)
{
	int left = 0;
	int right = SIZE-1;
	
	comparisons=0;

	while( left <= right )
	{
		int mid = (left+right)/2;

		comparisons++;
		if( key == arr[ mid ] )
			return true;

		if( key < arr[ mid ] )
			right = mid-1;
		else
			left = mid+1;

	}//repeat the above logic till subarray is valid
	return false;
}

bool_t updated_linear_search(int arr[], int key)
{
	int index;
	comparisons = 0;

	for( index = 0 ; index < SIZE && key >= arr[index] ; index++ )
	{
		comparisons++;
		if( key == arr[ index ] )
			return true;
	}

	return false;//key does not exists

}

bool_t rec_linear_search(int arr[], int key, int index)
{
	//base condition
	if( index == SIZE )
		return false;

	comparisons++;

	if( key == arr[ index ] )
		return true;
	else
		return ( rec_linear_search(arr, key, index+1));//modification
}

bool_t linear_search(int arr[], int key)
{
	int index;
	comparisons = 0;
	for( index = 0 ; index < SIZE ; index++ )
	{
		comparisons++;
		if( key == arr[ index ] )
			return true;
	}

	return false;//key does not exists
}

int rec_array_sum(int arr[], int index)
{
	if( index == SIZE )//base condition
		return 0;

	return ( arr[ index ] + rec_array_sum(arr,index+1) );//modification
}

int array_sum(int arr[])
{
	int sum = 0;
	int index;
	for( index = 0 ; index < SIZE ; index++ )
		sum += arr[ index ];

	return sum;
}

void display_array_elements(int arr[])
{
	int index;
	printf("array ele's are: ");
	for( index = 0 ; index < SIZE ; index++ )
	{
		printf("%4d", arr[index]);
	}
	printf("\n");
}

