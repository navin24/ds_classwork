/* filename: stack.c - this file contains definitions of stack functionalities */
#include"stack.h"

void init_stack(stack_t *ps)
{
	ps->top = -1;
}

bool_t is_stack_empty(stack_t *ps)
{
	return ( ps->top == -1 );
}

bool_t is_stack_full(stack_t *ps)
{
	return ( ps->top == SIZE-1 );
}

void push_element(stack_t *ps, node_t *element)
{
	ps->arr[ ++ps->top ] = element;
}

void pop_element(stack_t *ps)
{
	ps->top--;
}

node_t *peek_element(stack_t *ps)
{
	return ( ps->arr[ ps->top ] );
}
