//implementation of binary search tree
#include<stdio.h>
#include<stdlib.h>
#include"node.h"
#include"stack.h"


#define true 1
#define false 0

typedef struct
{
	node_t *root;
}bst;

void init_bst(bst *ptr);
bool_t is_bst_empty(bst *ptr);
void add_node(bst *ptr, int data);
void preorder(node_t *trav);
void inorder(node_t *trav);
void postorder(node_t *trav);
void nonrec_preorder(bst *ptr);
void nonrec_inorder(bst *ptr);
void nonrec_postorder(bst *ptr);


int main(void)
{
	bst t1;
	//50 20 90 85 10 45 30 100 15 75 95 120 5 50 
	init_bst(&t1);
	add_node(&t1, 50);
	add_node(&t1, 20);
	add_node(&t1, 90);
	add_node(&t1, 85);
	add_node(&t1, 10);
	add_node(&t1, 45);
	add_node(&t1, 30);
	add_node(&t1, 100);
	add_node(&t1, 15);
	add_node(&t1, 75);
	add_node(&t1, 95);
	add_node(&t1, 120);
	add_node(&t1, 5);
	add_node(&t1, 50);

	printf("preorder : "); preorder(t1.root); printf("\n");
	printf("preorder : "); nonrec_preorder(&t1); printf("\n");

	printf("inorder  : "); inorder(t1.root); printf("\n");
	printf("inorder  : "); nonrec_inorder(&t1); printf("\n");
	
	printf("postorder: "); postorder(t1.root); printf("\n");
	printf("postorder: "); nonrec_postorder(&t1); printf("\n");


	return 0;

}

void init_bst(bst *ptr)
{
	ptr->root = NULL;
}

bool_t is_bst_empty(bst *ptr)
{
	return ( ptr->root == NULL );
}

node_t *create_node(int data)
{
	//allocate memory dynamically for a node
	node_t *temp = (node_t *)malloc(sizeof(node_t));
	if( temp == NULL )
	{
		perror("malloc() failed !!!\n");
		exit(1);
	}
	//initialize members of the node
	temp->data = data;
	temp->left = NULL;
	temp->right = NULL;
	//initially node is not visited
	temp->visited = false;
	//return starting addr of newly created node to the calling function
	return temp;
}

void add_node(bst *ptr, int data)
{
	//create a newnode
	node_t *newnode = create_node(data);
	//if bst is empty - attach newly created node to root pointer
	if( is_bst_empty(ptr))
	{
		ptr->root = newnode;
	}
	else//if bst is not empty
	{
		//start traversal bst from root node
		node_t *trav = ptr->root;
		//find an appropriate position of a node by traversing it and add it into bst
		while(1)
		{
			if( data < trav->data )//node will be a part of its left subtree (of cur node)
			{
				//if node is not having left subtree i.e. if left subtree of a cur is empty
				if( trav->left == NULL )
				{
					trav->left = newnode;//attach newly created node as left child of cur node
					break;
				}
				else//if cur node is having left subtree
					trav = trav->left;//goto its left subtree
			}
			else//if( data >= trav->data )//node will be a part of its right subtree
			{
				//if node is not having right subtree i.e. if right subtree of a cur node is empty
				if( trav->right == NULL )
				{
					trav->right = newnode;//attach newly created node as a right child of cur node
					break;
				}
				else//if cur is having right subtree
					trav = trav->right;//goto its right subtree
			}
		}//end of while loop
	}//end of else
}

void preorder(node_t *trav)
{
	if( trav == NULL )
		return;
	//VLR
	printf("%4d", trav->data);//visit the node
	preorder(trav->left);//visit its subtree
	preorder(trav->right);//visit its right subtree
}

void inorder(node_t *trav)
{
	if( trav == NULL )
		return;
	//LVR
	inorder(trav->left);//visit its left subtree
	printf("%4d", trav->data);//visit node
	inorder(trav->right);//visit its right subtree
}

void postorder(node_t *trav)
{
	if( trav == NULL )
		return;
	//LRV
	postorder(trav->left);//visit its left subtree
	postorder(trav->right);//visit its right subtree
	printf("%4d", trav->data);//visit node
}

void nonrec_preorder(bst *ptr)
{
	node_t *trav = ptr->root;

	if( trav == NULL )//if bst is empty
		return;

	stack_t s;
	init_stack(&s);

	while( trav != NULL || !is_stack_empty(&s) )//outer while loop
	{
		while( trav != NULL )//inner while loop
		{
			//visit the node
			printf("%4d", trav->data);
			//if cur node is having right child - push it onto the stack
			if( trav->right != NULL )
				push_element(&s, trav->right);
			//goto its left subtree
			trav = trav->left;
		}

		//if stack is not empty
		if( !is_stack_empty(&s) )
		{
			//pop ele from the stack and catch it into trav
			trav = peek_element(&s);
			pop_element(&s);
		}
	}
}

void nonrec_inorder(bst *ptr)
{
	node_t *trav = ptr->root;

	if( trav == NULL )//if bst is empty
		return;

	stack_t s;
	init_stack(&s);

	while( trav != NULL || !is_stack_empty(&s) )//outer while loop
	{
		while( trav != NULL )//inner while loop
		{
			//push cur node onto the stack
			push_element(&s, trav);
			//goto its left subtree
			trav = trav->left;
		}

		//if stack is not empty
		if( !is_stack_empty(&s) )
		{
			//pop ele from the stack and catch it into trav
			trav = peek_element(&s);
			pop_element(&s);
			//visit the node
			printf("%4d", trav->data);
			//goto its right subtree
			trav = trav->right;
		}
	}
}

void nonrec_postorder(bst *ptr)
{
	node_t *trav = ptr->root;

	if( trav == NULL )//if bst is empty
		return;

	stack_t s;
	init_stack(&s);

	while( trav != NULL || !is_stack_empty(&s) )//outer while loop
	{
		while( trav != NULL )//inner while loop
		{
			//push cur node onto the stack
			push_element(&s, trav);
			//goto its left subtree
			trav = trav->left;
		}

		//if stack is not empty
		if( !is_stack_empty(&s) )
		{
			//pop ele from the stack and catch it into trav
			trav = peek_element(&s);
			pop_element(&s);
			
			//if node is having right subtree && it is not visited
			if( trav->right != NULL && trav->right->visited == false )
			{
				push_element(&s, trav);
				//goto to its right subtree
				trav = trav->right;
			}
			else
			{
				//visit the node & mark it is as visited
				printf("%4d", trav->data);
				trav->visited = true;
				trav = NULL;
			}
		}
	}
}
