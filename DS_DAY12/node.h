/* node.h : this file contains declaration of node structure */

#ifndef __NODE_H
#define __NODE_H

typedef int bool_t;

typedef struct node
{
	struct node *left;//4 bytes
	int data;//4 bytes
	struct node *right;//4 bytes
	bool_t visited;
}node_t;

#endif
