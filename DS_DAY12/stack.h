/* stack.h: this file contains declaration of stack structure & its functionalities */

#ifndef __STACK_H
#define __STACK_H

#define SIZE 20

#include"node.h"

typedef struct
{
	node_t *arr[ SIZE ];
	int top;
}stack_t;

void init_stack(stack_t *ps);
bool_t is_stack_empty(stack_t *ps);
bool_t is_stack_full(stack_t *ps);
void push_element(stack_t *ps, node_t *element);
void pop_element(stack_t *ps);
node_t *peek_element(stack_t *ps);

#endif

