#include<stdio.h>
#include<stdlib.h>

#define true 1
#define false 0

typedef int bool_t;

typedef struct node{
    int data;
    struct node *next;
}node_t;

typedef struct{
    node_t *head;
    int cnt;
}list_t;

void init_list(list_t *l1);
void add_last(list_t *list , int data);
void add_first(list_t *list , int data);
void add_specific_position(list_t *list ,int pos, int data);
int count_nodes(list_t *list);  //O(n) it takes order of n time to count
int node_count(list_t *list);  //O(1) while this takes order of 1 time to count
node_t *create_node(int data);
bool_t list_is_empty(list_t *list);
void display_list(list_t *list);
int main(void)
{
    list_t l1;
    init_list(&l1);
    

    add_last(&l1,10);
    add_last(&l1,20);
    add_last(&l1,30);
    display_list(&l1);
    add_specific_position(&l1,1,40);
    display_list(&l1);
    add_specific_position(&l1,3,50);
    display_list(&l1);
    add_specific_position(&l1,2,60);
    display_list(&l1);
    add_specific_position(&l1,6,70);
    display_list(&l1);
    add_specific_position(&l1,7,80);
    display_list(&l1);
    
   
    //add_first(&l1,10);
    //add_first(&l1,20);
//    add_first(&l1,30);
//    add_first(&l1,40);
//    add_first(&l1,50);
//    add_first(&l1,60);
//    add_first(&l1,70);
     //display_list(&l1);

     return 0;
}
void display_list(list_t *list)
{
    if(!list_is_empty(list))
    {
        // printf("no. of nodes in list    :   %d\n",count_nodes(list));
       
        node_t *trav = list->head;
        while(trav!=NULL)
        {
            printf("%d >> ",trav->data);
            trav = trav->next;
        }
        printf("null\n");
         printf("no. of nodes in list    :   %d\n",node_count(list));
    }
    else
    printf("List is Empty.\n");
}
void init_list(list_t *l1)
{
    l1->head=NULL;
    l1->cnt=0;
}
node_t *create_node(int data)
{
    node_t *temp = (node_t*)malloc(sizeof(node_t));
    if(temp==NULL)
    {
        perror("malloc() failed\n");
        exit(1);
    }
    temp->data = data;
    temp->next=NULL;

    return temp;
}
bool_t list_is_empty(list_t *list)
{
    return (list->head == NULL);
}
void add_last(list_t *list , int data)
{
    node_t *newnode=create_node(data);
    if(list_is_empty(list))
    {
        list->head = newnode;
        list->cnt++;
    }
    else
    {
        node_t *trav = list->head;
        while(trav->next!=NULL)
        {
            trav=trav->next;
        }
        trav->next=newnode; 
        list->cnt++;
    }
}

void add_first(list_t *list , int data)
{
    node_t *newnode=create_node(data);
    if(list_is_empty(list))
    {
        list->head = newnode;
        list->cnt++;
    }
    else
    {
        newnode->next=list->head;
        list->head=newnode;
        list->cnt++;
    }
}
void add_specific_position(list_t *list,int pos,int data)
{
    if(pos>=1 && pos < node_count(list)+1)
    {
        if(pos==1)
            add_first(list,data);
        else if(pos==node_count(list)+1)
            add_last(list,data);
        else
        {
            int i=1;
            node_t *newnode=create_node(data);
            node_t *trav=list->head;
            if(i<pos-1)
            {
                trav=trav->next;
                i++;
            }
            newnode->next = trav->next;
            trav->next=newnode;
            list->cnt++;           
        }
    }
    else
    printf("Wrong choice. Enter a valid position between 1-%d\n",node_count(list));
}

int count_nodes(list_t *list)
{
    int count=0;
    if(!list_is_empty(list))
    {
        node_t *trav = list->head;
        while(trav!=NULL)
        {
            count++;
            trav=trav->next;
        }
    }
    return count;
}
int node_count(list_t *list)
{
    return (list->cnt);
}