#include<stdio.h>
#include<stdlib.h>

#define true 1
#define false 0

#define SIZE 5
typedef int bool_t;

typedef struct
{
    int arr[SIZE];
    int rear;
    int front;
}queue_t;

int count =0;
int menu(void)
{
    printf("Linear Queue.\n");
    int choice;
    printf("0.Exit\n1.Enqueue\n2.Dequeue\n");
    printf("Enter your choice   :   ");
    scanf("%d",&choice);
    return choice;
}
void init_queue(queue_t *q);
bool_t is_queue_full(queue_t *q);
void enqueue(queue_t *q , int element);
void dequeue(queue_t *q);
int getfront(queue_t *q);
int main(void)
{
    queue_t q1;
    int ele;
   // printf("size    : %d\n",sizeof(q1));
   init_queue(&q1);
   while(1)
   {
       int ch = menu();
       switch(ch)
       {
        case 0:
            exit(1);
            break;
        case 1:
           //printf("size    : %d\n",sizeof(q1));
            if(!is_queue_full(&q1))
            {
                printf("Enter element to be added   :   ");
                scanf("%d",&ele);
                enqueue(&q1,ele);
                printf("Element added %d in the queue.\n ",ele);
            }
            else
                printf("List is Full!!\n");
            printf("Count %d\n",count);
            break;
        case 2:
            //printf("size    : %d\n",sizeof(q1));
            if(!is_queue_empty(&q1))
            {
                ele= getfront(&q1);
                dequeue(&q1);
                printf("Element deleted is %d \n ",ele);
            }
            else
                printf("List is empty.\n");
            break;
       }
   }
}

void init_queue(queue_t *q)
{
    q->rear=-1;
    q->front=-1;
    //printf("%d\t%d\n",q->rear,q->front);
}

bool_t is_queue_full(queue_t *q)
{
    return (q->front==(q->rear + 1) % SIZE);
}

bool_t is_queue_empty(queue_t *q)
{
    return (q->rear==-1 && q->front == q->rear);
}


void enqueue(queue_t *q , int element)
{
    q->rear= (q->rear+1) % SIZE;
    q->arr[q->rear] = element;
    ++count;
    if(q->front == -1)
        q->front=0;
}
void dequeue(queue_t *q)
{
    if(q->front == q->rear)
        init_queue(q);
    else
        q->front = (q->front + 1) % SIZE;
}

int getfront(queue_t *q)
{
    return q->arr[q->front];
}
