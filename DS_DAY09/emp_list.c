#include<stdio.h>
#include<stdlib.h>

typedef struct
{
	int empid;//4 bytes
	char name[32];//32 bytes
	int salary;//4 bytes
}emp_t;

typedef struct node
{
	emp_t data;//40 bytes
	struct node *next;//4 bytes
}node_t;

typedef struct
{
	node_t *head;
}emplist_t;


void accept_employee_record(emp_t *pe);
void display_employee_record(emp_t *pe);
void init_list(emplist_t *elist);
void add_node_at_last_position(emplist_t *elist, emp_t *pe);
node_t *create_node(emp_t *pe);
void display_emplist(emplist_t *elist);


int main(void)
{
	emp_t e1 = {1, "sachin", 1111 };
	emp_t e2 = {2, "sourav", 2222 };
	emp_t e3 = {3, "rahul", 3333 };
	emp_t e4 = {4, "yuvraj", 4444 };

	emplist_t elist;

	init_list(&elist);
	
	add_node_at_last_position(&elist, &e1);
	add_node_at_last_position(&elist, &e2);
	add_node_at_last_position(&elist, &e3);
	add_node_at_last_position(&elist, &e4);

	display_emplist(&elist);



	//accept_employee_record(&e1);
	//display_employee_record(&e1);

	return 0;
}

void display_emplist(emplist_t *elist)
{
	//if list is not empty
	if( elist->head != NULL )
	{
		node_t *trav = elist->head;

		printf("list of employees is:\n");
		//traverse the list till last node (including it)
		while( trav != NULL )
		{
			display_employee_record(&trav->data);
			trav = trav->next;
		}
	}
	else
		printf("emplist is empty !!!\n");

}

node_t *create_node(emp_t *pe)
{
	//alloacte memory dynamically for a node
	node_t *temp = (node_t *)malloc(sizeof(node_t));
	if( temp == NULL )
	{
		perror("malloc() failed !!!\n");
		exit(1);
	}

	//initialize members of the node
	temp->data = *pe;
	temp->next = NULL;
	//return starting addr of newly created node
	return temp;
}

void add_node_at_last_position(emplist_t *elist, emp_t *pe)
{
	//create a newnode
	node_t *newnode = create_node(pe);

	//if list is empty -- attach newly created node to the head
	if( elist->head == NULL )
	{
		elist->head = newnode;
	}
	else//if list is not empty
	{
		//start traversal from the fisr node
		node_t *trav = elist->head;
		//traverse the list till last node
		while( trav->next != NULL )
			trav = trav->next;
		//attach newly created node to the last node
		trav->next = newnode;
	}
}

void init_list(emplist_t *elist)
{
	elist->head = NULL;
}

void accept_employee_record(emp_t *pe)
{
	printf("enter empid, name & salary: ");
	scanf("%d %s %d", &pe->empid, pe->name, &pe->salary);
}

void display_employee_record(emp_t *pe)
{
	printf("%-10d %-20s %-10d\n", pe->empid, pe->name, pe->salary);
}
