//implementation of static stack
#include<stdio.h>
#include<stdlib.h>

#define SIZE 5

typedef int bool_t;

typedef struct
{
	int arr[SIZE];
	int top;
}stack_t;

	
void init_stack(stack_t *ps);
bool_t is_stack_full(stack_t *ps);
bool_t is_stack_empty(stack_t *ps);
void push_element(stack_t *ps, int ele);
int peek_element(stack_t *ps);
void pop_element(stack_t *ps);


int menu(void)
{
	int choice;
	printf("static stack\n");
	printf("0. exit\n");
	printf("1. push element\n");
	printf("2. pop element\n");
	printf("3. peek element\n");
	printf("enter the choice: ");
	scanf("%d", &choice);
	return choice;
}

enum menu_options{ EXIT, PUSH, POP, PEEK };

int main(void)
{
	stack_t s;
	int choice;
	int ele;

	init_stack(&s);

	while(1)
	{
		choice = menu();
		switch( choice )
		{
			case EXIT:
				exit(0);

			case PUSH:
				//step1 - check stack is not full
				if( !is_stack_full(&s) )
				{
					printf("enter an ele: ");
					scanf("%d", &ele);
					push_element(&s, ele);
				}
				else
					printf("stack overflow !!!\n");
				break;

			case POP:
				//step1 - check stack is not empty
				if( !is_stack_empty(&s) )
				{
					ele = peek_element(&s);
					pop_element(&s);
					printf("popped ele is: %d\n", ele);
				}
				else
					printf("stack underflow !!!\n");
				break;

			case PEEK:
				//step1 - check stack is not empty
				if( !is_stack_empty(&s) )
				{
					ele = peek_element(&s);
					printf("topmost ele is: %d\n", ele);
				}
				else
					printf("stack underflow !!!\n");
				break;
		}//end of switch control block
	}//end of while loop

	//printf("sizeof(stack_t): %d bytes\n", sizeof(stack_t));
	return 0;
}

void init_stack(stack_t *ps)
{
	/*
	int index;
	for( index = 0 ; index < SIZE ; index++ )
		ps->arr[ index ] = 0;
	*/

	//initialize value of top as -1
	ps->top = -1;

}

bool_t is_stack_full(stack_t *ps)
{
	return ( ps->top == SIZE-1 );
}

bool_t is_stack_empty(stack_t *ps)
{
	return ( ps->top == -1 );
}

void push_element(stack_t *ps, int ele)
{
	//step2 - increment the value of top by 1
	ps->top++;
	//step3 - insert/add an ele onto the stack at top position
	ps->arr[ ps->top ] = ele;
	
	//OR
	//ps->arr[ ++ps->top ] = ele;
}

int peek_element(stack_t *ps)
{
	//step2 - return the value of an ele which is at top position
	return ( ps->arr[ ps->top ] );
}

void pop_element(stack_t *ps)
{
	//step2 - decrement the value of top by 1
	ps->top--;
}
