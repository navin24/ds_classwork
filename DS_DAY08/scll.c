//singly circular linked list operations

#include<stdio.h>
#include<stdlib.h>

typedef int bool_t;

typedef struct node
{
    int data;
    struct node *next;
}node_t;

typedef struct
{
    node_t *head;
    int cnt;
}list_t;

//function declarations
void init_list(list_t *list);
void add_node_at_last_position(list_t *list, int data);
void add_node_at_first_position(list_t *list, int data);
void delete_node_at_first_position(list_t *list);
void delete_node_at_last_position(list_t *list);
node_t *create_node(int data);
bool_t is_list_empty(list_t *list);
void display_list(list_t *list);
int count_nodes(list_t *list);
int get_nodes_count(list_t *list);
void free_list(list_t *list);

int main(void)
{
    list_t l1;

    init_list(&l1);
    add_node_at_last_position(&l1, 11);
    add_node_at_last_position(&l1, 22);
    add_node_at_last_position(&l1, 33);
    add_node_at_last_position(&l1, 44);
    add_node_at_last_position(&l1, 55);
    add_node_at_last_position(&l1, 66);

    display_list(&l1);
		delete_node_at_last_position(&l1);
		//delete_node_at_first_position(&l1);
		//add_node_at_first_position(&l1, 5);
		display_list(&l1);

		if( !is_list_empty(&l1))
			free_list(&l1);

    return  0;
}

void init_list(list_t *list)
{
    list->head = NULL;
    list->cnt = 0;
}

bool_t is_list_empty(list_t *list)
{
    return ( list->head == NULL );
}

node_t *create_node(int data)
{
    //allocate memory dyanamically for node
    node_t *temp = (node_t *)malloc(sizeof(node_t));
    if( temp == NULL )
    {
        perror("malloc() failed !!!\n");
        exit(1);
    }
    //initialize members of the node
    temp->data = data;
    temp->next = NULL;
    //return the starting addr of newly created node to the calling function
    return temp;
}

void display_list(list_t *list)
{
    //if list is not empty
    if( !is_list_empty(list) )
    {
        //start traversal from first node
        node_t *trav = list->head;
        printf("list is: ");
        //traverse the list till last node (including last node)
        do
        {
            printf("%4d", trav->data);//visit data part of cur node
            trav = trav->next;//move trav towards its next node
        }while( trav != list->head );
        printf("\n");
				//printf("no. of nodes in a list are: %d\n", count_nodes(list));
				printf("no. of nodes in a list are: %d\n", get_nodes_count(list));
    }
    else
    {
        printf("list is empty !!!\n");
    }
    
}

void add_node_at_last_position(list_t *list, int data)
{
    //create a newnode
    node_t *newnode = create_node(data);
    //if list is empty -- attach newly created node to the head
    if( is_list_empty(list) )
    {
        list->head = newnode;
        newnode->next = list->head;
        list->cnt++;
    }
    else//if list is not empty
    {
        //start traveral from first node
        node_t *trav = list->head;
        //traverse the list till last node
        while( trav->next != list->head )
            trav = trav->next;

        //attach newly created node to the last node
        trav->next = newnode;
        newnode->next = list->head;
        list->cnt++;
    }
}

void add_node_at_first_position(list_t *list, int data)
{
    //create a newnode
    node_t *newnode = create_node(data);
    //if list is empty -- attach newly created node to the head
    if( is_list_empty(list) )
    {
        list->head = newnode;
        newnode->next = list->head;
        list->cnt++;
    }
    else//if list is not empty
    {
        //start traveral from first node
        node_t *trav = list->head;
        //traverse the list till last node
        while( trav->next != list->head )
            trav = trav->next;
				//store an addr of cur first node into the next part of newly created node
				newnode->next = list->head;
				//attach newly created node to the head
				list->head = newnode;
				//update next part of last node to the newly added node at first pos
				trav->next = list->head;
        list->cnt++;
    }
}

int count_nodes(list_t *list)
{
	int cnt=0;

	//if list is not empty
	if( !is_list_empty(list))
	{
		//start traversal from the first node
		node_t *trav = list->head;
		//traverse the list till last node (including last node)
		do
		{
			cnt++;
			trav = trav->next;
		}while( trav != list->head );
	}
	//return the count
	return cnt;
}

int get_nodes_count(list_t *list)
{
	return ( list->cnt );
}

void delete_node_at_first_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head == list->head->next )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//start traversal from the first node
			node_t *trav = list->head;
			//traverse the list till last node
			while( trav->next != list->head )
				trav = trav->next;

			//attach cur second node to the head
			list->head = list->head->next;
			//delete the prev first node
			free(trav->next);
			//update an addr modified first node into next part of last node
			trav->next = list->head;
			list->cnt--;
		}
	}
	else
	{
		printf("list is empty !!!\n");
	}
}

void delete_node_at_last_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head == list->head->next )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//start traversal from the first node
			node_t *trav = list->head;
			//traverse the list till second last node
			while( trav->next->next != list->head )
				trav = trav->next;

			//delete the last node
			free(trav->next);
			//store an addr of first node into the next part cur second last node
			trav->next = list->head;
			list->cnt--;
		}
	}
	else
	{
		printf("list is empty !!!\n");
	}
}
void free_list(list_t *list)
{
	while( !is_list_empty(list) )
			delete_node_at_last_position(list);
		//delete_node_at_first_position(list);

	printf("list freed sucessfully...\n");
}

