#include<stdio.h>
#include<stdlib.h>

typedef int bool_t;

typedef struct node
{
	struct node *prev;
	int data;
	struct node *next;
}node_t;

typedef struct
{
	node_t *head;
	int cnt;
}list_t;

	
void init_list(list_t *list);
void add_node_at_last_position(list_t *list, int data);
void add_node_at_first_position(list_t *list, int data);
void add_node_at_specific_position(list_t *list, int pos, int data);
void delete_node_at_first_position(list_t *list);
void delete_node_at_last_position(list_t *list);
void delete_node_at_specific_position(list_t *list, int pos);
bool_t is_list_empty(list_t *list);
void display_list(list_t *list);
int get_nodes_count(list_t *list);


int main(void)
{
	list_t l1;
	int pos;

	init_list(&l1);
	//printf("sizeof(node_t): %d bytes\n", sizeof(node_t));

	add_node_at_last_position(&l1, 11);
	add_node_at_last_position(&l1, 22);
	add_node_at_last_position(&l1, 33);
	add_node_at_last_position(&l1, 44);
	add_node_at_last_position(&l1, 55);
	add_node_at_last_position(&l1, 66);

	display_list(&l1);

	//delete_node_at_first_position(&l1);
	//delete_node_at_last_position(&l1);

	
	//add_node_at_first_position(&l1, 5);

	while(1)
	{
		//accept position from the user 
		printf("enter the position: ");
		scanf("%d", &pos);

		if( pos >= 1 && pos <= get_nodes_count(&l1) )
			break;

		printf("invalid position...\n");

	}

	delete_node_at_specific_position(&l1, pos);
	//add_node_at_specific_position(&l1, pos, 99);
	

	display_list(&l1);


	return 0;
}

int get_nodes_count(list_t *list)
{
	return ( list->cnt );
}

void display_list(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//start traversal from the first node
		node_t *trav = list->head;
		node_t *temp = NULL;
		printf("list in forward dir is   : ");
		//traverse the list till last node (including last node)
		while( trav != NULL )
		{
			temp = trav;
			printf("%4d", trav->data);
			trav = trav->next;
		}
		printf("\n");
		//start traversal from last node for backward traversal
		trav = temp;
		printf("list in a backward dir is: ");
		//traverse the list till first node (including first node)
		while( trav != NULL )
		{
			printf("%4d", trav->data);
			trav = trav->prev;//move trav pointer towards its prev node
		}
		printf("\n");
		printf("no. of nodes in a list are: %d\n", get_nodes_count(list));
	}
	else
		printf("list is empty !!!\n");

}

void init_list(list_t *list)
{
	list->head = NULL;
	list->cnt = 0;
}

bool_t is_list_empty(list_t *list)
{
	return ( list->head == NULL );
}
	
node_t *create_node(int data)
{
	//allocate memory dynamically for a node
	node_t *temp = (node_t *)malloc(sizeof(node_t));
	if( temp == NULL )
	{
		perror("malloc() failed !!!\n");
		exit(1);
	}

	//initialize its members
	temp->data = data;
	temp->prev = NULL;
	temp->next = NULL;

	//return starting addr of dynamically allocated node to the calling function
	return temp;
}

/*
	 add_last() function:
	 best case		: O(1) - if list is empty
	 worst case		: O(n) - if list is not empty
	 average case	: O(n)
	 */
	 
void add_node_at_last_position(list_t *list, int data)
{
	//create a newnode
	node_t *newnode = create_node(data);

	//if list is empty -- attach newly created node to the head
	if( is_list_empty(list))
	{
		list->head = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//start traversal from first node
		node_t *trav = list->head;
		//traverse the list till last node
		while( trav->next != NULL )
			trav = trav->next;

		//attach newly created node to the last node
		trav->next = newnode;
		//store an addr of cur last node into the prev part of newly created node
		newnode->prev = trav;
		list->cnt++;
	}
}

void add_node_at_first_position(list_t *list, int data)
{
	//create a newnode
	node_t *newnode = create_node(data);

	//if list is empty -- attach newly created node to the head
	if( is_list_empty(list))
	{
		list->head = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//store an addr of newly  created node to the prev part of cur first node
		list->head->prev = newnode;
		//store an addr of cur first node into the next part of newly created node
		newnode->next = list->head;
		//attach newly created node to the head
		list->head = newnode;
		list->cnt++;
	}
}

void add_node_at_specific_position(list_t *list, int pos, int data)
{
	if( pos == 1)
		add_node_at_first_position(list, data);
	else
	if( pos == get_nodes_count(list) + 1 )
		add_node_at_last_position(list, data);
	else//if pos is inbetween position
	{
		//create a newnode
		node_t *newnode = create_node(data);
		//start traversal from the first node
		node_t *trav = list->head;
		int i = 1;
		//traverse the list till (pos-1)th node
		while( i < pos-1 )
		{
			i++;
			trav = trav->next;
		}

		//store an addr of (pos-1)th node into the prev part of newly created node
		newnode->prev = trav;
		//store an addr of cur (pos)th node into the next part of newly created node
		newnode->next = trav->next;
		//store an addr of newly created node into the prev part of cur (pos)th node
		trav->next->prev = newnode;
		//store an addr of newly created node into the next part of (pos-1)th node
		trav->next = newnode;
		list->cnt++;
	}
}

void delete_node_at_first_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head->next == NULL )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//attach cur second node to the head
			list->head = list->head->next;
			//delete cur first node
			free(list->head->prev);
			//make prev part of old second node as a NULL
			list->head->prev = NULL;
			list->cnt--;
		}
	}
	else
		printf("list is empty !!!\n");
}

void delete_node_at_last_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list))
	{
		//if list contains only one node
		if( list->head->next == NULL )
		{
			//delete the node and make head as NULL & cnt as 0
			free(list->head);
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//start traversal from the first node
			node_t *trav = list->head;
			//travserse the list till second last node
			while( trav->next->next != NULL )
				trav = trav->next;

			//delete the last node
			free(trav->next);
			//make next part of second last node as NULL
			trav->next = NULL;
			list->cnt--;
		}
	}
	else
		printf("list is empty !!!\n");
}

void delete_node_at_specific_position(list_t *list, int pos)
{
	if( pos == 1 )
		delete_node_at_first_position(list);
	else
	if( pos == get_nodes_count(list) )
		delete_node_at_last_position(list);
	else//if pos is inbetween position
	{
		node_t *trav = list->head, *temp = NULL;
		int i = 1;
		while( i < pos-1 )
		{
			i++;
			trav = trav->next;
		}

		//store an addr of node into a temp which is to be deleted
		temp = trav->next;
		//store an addr of (pos-1)th node into prev part of (pos+1)th node
		temp->next->prev = trav;
		//store an addr of (pos+1)th node into the next part of (pos-1)th node
		trav->next = temp->next;
		//delete the node
		free(temp);
		temp = NULL;
		list->cnt--;
	}
}
