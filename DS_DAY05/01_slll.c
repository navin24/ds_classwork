#include<stdio.h>
#include<stdlib.h>

#define true 1
#define false 0

typedef int bool_t;

typedef struct node{
    int data;
    struct node *next;
}node_t;

typedef struct{
    node_t *head;
}list_t;

void init_list(list_t *l1);
void add_last(list_t *list , int data);
node_t *create_node(int data);
bool_t list_is_empty(list_t *list);
void display_list(list_t *list);
int main(void)
{
    list_t l1;
    init_list(&l1);

    add_last(&l1,10);
    add_last(&l1,20);
    add_last(&l1,30);
    add_last(&l1,40);
    add_last(&l1,50);
    display_list(&l1);

     return 0;
}
void display_list(list_t *list)
{
    if(!list_is_empty(list))
    {
        node_t *trav = list->head;
        while(trav!=NULL)
        {
            printf("%d >> ",trav->data);
            trav = trav->next;
        }
        printf("null\n");
    }
    else
    printf("List is Empty.\n");
}
void init_list(list_t *l1)
{
    l1->head=NULL;
}
node_t *create_node(int data)
{
    node_t *temp = (node_t*)malloc(sizeof(node_t));
    if(temp==NULL)
    {
        perror("malloc() failed\n");
        exit(1);
    }
    temp->data = data;
    temp->next=NULL;

    return temp;
}
bool_t list_is_empty(list_t *list)
{
    return (list->head == NULL);
}
void add_last(list_t *list , int data)
{
    node_t *newnode=create_node(data);
    if(list_is_empty(list))
    {
        list->head = newnode;
    }
    else
    {
        node_t *trav = list->head;
        while(trav->next!=NULL)
        {
            trav=trav->next;
        }
        trav->next=newnode; 
    }
}

