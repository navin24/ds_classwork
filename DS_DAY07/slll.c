//singly linear linked list operations:
#include<stdio.h>
#include<stdlib.h>

#define true 1
#define false 0

typedef int bool_t;

typedef struct node
{
	int data;//4 bytes
	struct node *next;//4 bytes
}node_t;

typedef struct
{
	node_t *head;//4 bytes
	int cnt;
}list_t;

//function declarations:
void init_list(list_t *list);
bool_t is_list_empty(list_t *list);
void add_node_at_last_position(list_t *list, int data);
void add_node_at_first_position(list_t *list, int data);
void add_node_at_specific_position(list_t *list, int pos, int data);
void delete_node_at_first_position(list_t *list);
void delete_node_at_last_position(list_t *list);
void delete_node_at_specific_position(list_t *list, int pos);
void display_list(list_t *list);
int count_nodes(list_t *list);
node_t *create_node(int data);
int get_count_of_nodes_list(list_t *list);
void free_list(list_t *list);
bool_t search_and_delete(list_t *list, int key);
node_t *search_node(list_t *list, node_t **prev, int key);
void display_reverse(node_t *trav);
void reverse_list(list_t *list);
void selection_sort(list_t *list);

int menu(void)
{
	int choice;

	printf("singly linear linked list\n");
	printf("0. exit\n");
	printf("1. add node at last position\n");
	printf("2. add node at first position\n");
	printf("3. add node at specific position\n");
	printf("4. delete node at first position\n");
	printf("5. delete node at last position\n");
	printf("6. delete node at specific position\n");
	printf("7. display list\n");
	printf("8. search & delete node\n");
	printf("9. display list in a reverse order\n");
	printf("10. reverse the list\n");
	printf("11. selection sort\n");

	printf("enter the choice: ");
	scanf("%d", &choice);

	return choice;
}

enum menu_options{ EXIT, ADDLAST, ADDFIRST, ADDATPOS, DELFIRST, DELLAST, 
DELATPOS, DISPLAY, SEARCH_DEL, DISP_REV, REVERSE_LIST, SEL_SORT };

//entry point function
int main(void)
{
	//create an empty list
	list_t l1;
	int pos;
	int data;
	int key;

	init_list(&l1);

	while(1)
	{
		int choice = menu();
		switch(choice)
		{
			case EXIT:
				if( !is_list_empty(&l1))
					free_list(&l1);

				exit(0);

			case ADDLAST:
				printf("enter the data: ");
				scanf("%d", &data);
				add_node_at_last_position(&l1, data);
				break;

			case ADDFIRST:
				printf("enter the data: ");
				scanf("%d", &data);
				add_node_at_first_position(&l1, data);
				break;

			case ADDATPOS:
				while( 1 )
				{
					//accept position from the user
					printf("enter the position: ");
					scanf("%d", &pos);

					//validate the position
					if( pos >= 1 && pos <= get_count_of_nodes_list(&l1) + 1  )
						break;

					printf("invalid position !!!\n");
				}

				printf("enter the data: ");
				scanf("%d", &data);
				add_node_at_specific_position(&l1, pos, data);
				break;

			case DELFIRST:
				delete_node_at_first_position(&l1);
				break;

			case DELLAST:
				delete_node_at_last_position(&l1);
				break;

			case DELATPOS:
				while( 1 )
				{
					//accept position from the user
					printf("enter the position: ");
					scanf("%d", &pos);

					//validate the position
					if( pos >= 1 && pos <= get_count_of_nodes_list(&l1) )
						break;

					printf("invalid position !!!\n");
				}

				delete_node_at_specific_position(&l1, pos);
				break;
			case DISPLAY:
				display_list(&l1);
				break; 
			
			case SEARCH_DEL:
				printf("enter the key: ");
				scanf("%d", &key);
				
				if( search_and_delete(&l1, key) )
					printf("key is found and deleted...\n");
				else
					printf("key is not found\n");
				break;

			case DISP_REV:
				printf("list in a reverse order is: ");
				display_reverse(l1.head);
				printf("\n");
				break;

			case REVERSE_LIST:
				reverse_list(&l1);
				break;

			case SEL_SORT:
				selection_sort(&l1);
				break;

		}//end of switch control block
	}//end of while loop

	return 0;
}

//function definitions
int count_nodes(list_t *list)
{
	int cnt = 0;
	//if list is not empty
	if( !is_list_empty(list))
	{
		//start traversal from the first node
		node_t *trav = list->head;
		//traverse the list till last node
		while( trav != NULL )
		{
			cnt++;
			trav = trav->next;//move trav towards its next node
		}
	}

	return cnt;
}

int get_count_of_nodes_list(list_t *list)
{
	return ( list->cnt );
}

node_t *create_node(int data)
{
	//allocate memory dynamically for a node
	node_t *temp = (node_t *)malloc( sizeof(node_t) );
	if( temp == NULL )
	{
		perror("malloc() failed !!!\n");
		exit(1);
	}

	//initialalize its members
	temp->data = data;
	temp->next = NULL;
	
	//return an addr of dynamically created node to the calling function
	return temp;
}

void display_list(list_t *list)
{
	if( !is_list_empty(list) )
	{
		node_t *trav = list->head;
		printf("head -> ");
		while( trav != NULL )
		{
			printf("%d -> ", trav->data);//visit the data part of cur node
			trav = trav->next;//move trav pointer towards its next node
		}
		printf(" null\n");
		//printf("no. of nodes in a list are: %d\n", count_nodes(list));
		//printf("no. of nodes in a list are: %d\n", list->cnt);
		printf("no. of nodes in a list are: %d\n", get_count_of_nodes_list(list));


	}
	else
		printf("list is empty !!!\n");
}

bool_t is_list_empty(list_t *list)
{
	return ( list->head == NULL );
}

void init_list(list_t *list)
{
	list->head = NULL;
	list->cnt = 0;
}

void add_node_at_first_position(list_t *list, int data)
{
	//create a newnode
	node_t *newnode = create_node(data);

	//if list is empty -- attach newly created node to the head
	if( is_list_empty(list) )
	{
		//store an addr of newly created node to the head
		list->head = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//store an addr of cur first node into the next part of newly created node
		newnode->next = list->head;
		//store an addr of newly created node into the head
		list->head = newnode;
		list->cnt++;
	}
}

void add_node_at_last_position(list_t *list, int data)
{
	//create a newnode
	node_t *newnode = create_node(data);

	//if list is empty -- attach newly created node to the head
	if( is_list_empty(list) )
	{
		//store an addr of newly created node to the head
		list->head = newnode;
		list->cnt++;
	}
	else//if list is not empty
	{
		//start traversal from first node
		node_t *trav = list->head;
		//traverse the list till last node
		while( trav->next != NULL )
		{
			trav = trav->next;//move trav pointer towards its next node
		}

		//attach newly created node to the last node
		trav->next = newnode;
		list->cnt++;
	}
}

void add_node_at_specific_position(list_t *list, int pos, int data)
{
	if( pos == 1 )//if pos is the first position
		add_node_at_first_position(list, data);
	else 
	if( pos == get_count_of_nodes_list(list) + 1 )//if pos is the last position
		add_node_at_last_position(list, data);
	else//if pos is in between position
	{
		//create a newnode
		node_t *newnode = create_node(data);
		//start traversal from the first node
		node_t *trav = list->head;
		int i = 1;

		//traverse the list till (pos-1)th node
		while( i < pos-1 )
		{
			i++;
			trav = trav->next;
		}

		//store an addr of cur (pos)th node into next part of newly created node		
		newnode->next = trav->next;
		//store an addr of newly created node into next part of (pos-1)th node
		trav->next = newnode;
		list->cnt++;
	}
}

void delete_node_at_first_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list) )
	{
		//if list contains only one node
		if( list->head->next == NULL )
		{
			//delete the node
			free(list->head);
			//make head as NULL & cnt as 0
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//store an addr of cur first node into a temp which is to be deleted
			node_t *temp = list->head;
			//attach cur second node to the head
			list->head = list->head->next;
			//delete the cur first node
			free(temp);
			temp = NULL;
			list->cnt--;
		}
	}
	else
	{
		printf("list is empty !!!\n");
	}
}

void free_list(list_t *list)
{
	//while list not becomes empty
	while( !is_list_empty(list) )
		delete_node_at_last_position(list);
		//delete_node_at_first_position(list);

	printf("list freed sucessfully !!!\n");
}

void delete_node_at_last_position(list_t *list)
{
	//if list is not empty
	if( !is_list_empty(list) )
	{
		//if list contains only one node
		if( list->head->next == NULL )
		{
			//delete the node
			free(list->head);
			//make head as NULL & cnt as 0
			list->head = NULL;
			list->cnt = 0;
		}
		else//if list contains more than one nodes
		{
			//start traversal from first node
			node_t *trav = list->head;
			//traverse the list till second last node
			while( trav->next->next != NULL )
				trav = trav->next;

			//delete the last node
			free(trav->next);
			//make next part of cur second last node as NULL
			trav->next = NULL;

			list->cnt--;
		}
	}
	else
	{
		printf("list is empty !!!\n");
	}
}

void delete_node_at_specific_position(list_t *list, int pos)
{
	if( pos == 1 )//if pos is the first position
		delete_node_at_first_position(list);
	else 
	if( pos == get_count_of_nodes_list(list) )//if pos is the last position
		delete_node_at_last_position(list);
	else//if pos is inbetween position
	{
		//start traversal from the first node
		node_t *trav = list->head;
		node_t *temp = NULL;
		int i = 1;

		//traverse the list till (pos-1)th node
		while( i < pos-1 )
		{
			i++;
			trav = trav->next;
		}

		//store an addr of the node into the temp which is to be deleted
		temp = trav->next;
		//store an addr of (pos+1)th node into next part of (pos-1)th node
		trav->next = trav->next->next;

		//delete the node
		free(temp);
		temp = NULL;
		list->cnt--;
	}
}

/* if key node is found: this function should return addr of a node as
well as addr of its prev node
if key node is not found: it should return NULL */
node_t *search_node(list_t *list, node_t **prev, int key)
{
	//start traversal from the first node
	node_t *trav = list->head;
	//traverse the list last node (including it)
	while( trav != NULL )
	{
		if( key == trav->data )
			return trav;

		*prev = trav;
		trav = trav->next;
	}

	*prev = NULL;
	return NULL;
}

bool_t search_and_delete(list_t *list, int key)
{
	node_t *prev = NULL;
	node_t *temp = search_node(list, &prev, key);
	
	if( temp == NULL )
		return false;

	if( prev == NULL )//if key node is the first node
	{
		//printf("temp->data: %d\n", temp->data);
		//attach cur second node to the head
		list->head = temp->next;
		//delete the node
		free(temp);
		temp = NULL;
		list->cnt--;
	}
	else
	{
		//printf("temp->data: %d\t prev->data: %d\n", temp->data, prev->data);
		prev->next = temp->next;
		free(temp);
		temp = NULL;
		list->cnt--;
	}

	return true;
}

void display_reverse(node_t *trav)
{
	if( trav == NULL )
		return;

	display_reverse(trav->next);
	printf("%4d", trav->data);
}


void reverse_list(list_t *list)
{
	node_t *t1 = list->head;
	node_t *t2 = t1->next;
	t1->next = NULL;

	while( t2 != NULL )
	{
		node_t *t3 = t2->next;
		t2->next = t1;//reverse the link of pair of nodes
		t1 = t2;//move t1 towards its next node
		t2 = t3;//move t2 towards its next node
	}
	list->head = t1;
}

//Q. Write a recursive function to reverse a linked list

void selection_sort(list_t *list)
{
	node_t *t1 = NULL;
	node_t *t2 = NULL;

	for( t1 = list->head ; t1->next != NULL ; t1 = t1->next )
	{
		for( t2 = t1->next ; t2 != NULL ; t2 = t2->next )
		{
			if( t1->data > t2->data )
			{
				//swap data part of the nodes
				int temp = t1->data;
				t1->data = t2->data;
				t2->data = temp;
			}
		}
	}
}










