#include<stdio.h>
#include<stdlib.h>

typedef int bool_t;

typedef struct node
{
    int data;
    struct node *next;
}node_t;

typedef struct
{
    node_t *head;
    int cnt;
}list_t;

void init_list(list_t *list);
void add_node_at_last_position(list_t *list, int data);
node_t *create_node(int data);
bool_t is_list_empty(list_t *list);
void display_list(list_t *list);

int main(void)
{
    list_t l1;

    init_list(&l1);
    add_node_at_last_position(&l1, 11);
    add_node_at_last_position(&l1, 22);
    add_node_at_last_position(&l1, 33);
    add_node_at_last_position(&l1, 44);
    add_node_at_last_position(&l1, 55);
    add_node_at_last_position(&l1, 66);

    display_list(&l1);




    return  0;
}

void init_list(list_t *list)
{
    list->head = NULL;
    list->cnt = 0;
}

bool_t is_list_empty(list_t *list)
{
    return ( list->head == NULL );
}

node_t *create_node(int data)
{
    //allocate memory dyanamically for node
    node_t *temp = (node_t *)malloc(sizeof(node_t));
    if( temp == NULL )
    {
        perror("malloc() failed !!!\n");
        exit(1);
    }
    //initialize members of the node
    temp->data = data;
    temp->next = NULL;
    //return the starting addr of newly created node to the calling function
    return temp;
}

void display_list(list_t *list)
{
    //if list is not empty
    if( !is_list_empty(list) )
    {
        //start traversal from first node
        node_t *trav = list->head;
        printf("list is: ");
        //traverse the list till last node (including last node)
        do
        {
            printf("%4d", trav->data);//visit data part of cur node
            trav = trav->next;//move trav towards its next node
        }while( trav != list->head );
        printf("\n");
    }
    else
    {
        printf("list is empty !!!\n");
    }
    
}

void add_node_at_last_position(list_t *list, int data)
{
    //create a newnode
    node_t *newnode = create_node(data);
    //if list is empty -- attach newly created node to the head
    if( is_list_empty(list) )
    {
        list->head = newnode;
        newnode->next = list->head;
        list->cnt++;
    }
    else//if list is not empty
    {
        //start traveral from first node
        node_t *trav = list->head;
        //traverse the list till last node
        while( trav->next != list->head )
            trav = trav->next;

        //attach newly created node to the last node
        trav->next = newnode;
        newnode->next = list->head;
        list->cnt++;
    }
}


