#include<stdio.h>

#define SIZE 9

void print_array(int arr[]);
int sum_array(int arr[]);
int rec_sum_array(int arr[],int i);
int main(void)
{
    int arr[SIZE]={10,40,20,30,50,60,80,70,90};
    print_array(arr);
    printf("Sum of array elements   :   %d\n",sum_array(arr));
    printf("Sum of array elements   :   %d\n",rec_sum_array(arr,0));
    return 0;
}
void print_array(int arr[])
{
    int i;
    for(i=0;i<SIZE;i++)
    {
        printf("Arr[%d] :   %d\n",i,arr[i]);
    }
}
int sum_array(int arr[])
{
    int i,sum=0;
    for(i=0;i<SIZE;i++)
    {
        sum=sum+arr[i];
    }
    return sum;
}

int rec_sum_array(int arr[],int i)
{
    while(i<SIZE)
    {
        return (arr[i] + rec_sum_array(arr,i+1));
    }
    return 0;
}