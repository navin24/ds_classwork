//stack applications
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 10

typedef int bool_t;

typedef struct
{
	int arr[SIZE];
	int top;
}stack_t;

	
void init_stack(stack_t *ps);
bool_t is_stack_full(stack_t *ps);
bool_t is_stack_empty(stack_t *ps);
void push_element(stack_t *ps, int ele);
int peek_element(stack_t *ps);
void pop_element(stack_t *ps);
void infix_to_postfix(char *in, char *post);
void infix_to_prefix(char *in, char *pre);
bool_t is_operand(char ch);
int priority(char opr);
char *my_strrev(char *str);


int main(void)
{
	char infix[64] = "a*b/c*d/e+f-h";
	//char infix[64] = "(a+b)*(c/d)*e/f+g";
	char postfix[64] = " ";
	char prefix[64] = " ";


	printf("infix expression is: %s\n", infix);
	//infix_to_postfix(infix, postfix);
	infix_to_prefix(infix, prefix);
	//printf("postfix expression is: %s\n", postfix);
	printf("prefix expression is: %s\n", prefix);

	return 0;
}

bool_t is_operand(char ch)
{
	return ( ( ch >= 65 && ch <= 90 ) || ( ch >= 97 && ch <= 122 ) || ( ch >= 48 && ch <= 57 ) );
}

int priority(char opr)
{
	switch(opr)
	{
		case '*':
		case '/':
		case '$':
		case '%':
		case '^': return 2;

		case '+':
		case '-': return 1;

		case '(': return 0;
	}
}

char *my_strrev(char *str)
{
	char *ptr = str;
	char *left = str;
	char *right = str;

	//move right pointer at the end of the string
	while( *right )
		right++;

	right--;
	while( left <= right )
	{
		char t = *left;
		*left = *right;
		*right = t;

		left++;
		right--;
	}

	return ptr;
}

void infix_to_prefix(char *in, char *pre)
{
	stack_t s;
	int i;
	int j=0;

	init_stack(&s);

	//step1: start scanning infix expression from right to left
	for( i = strlen(in)-1 ; i >= 0  ; i-- )
	{
		//step2: if( cur ele is an operand ) -- append it into the prefix expression
		if( is_operand(in[i]))
		{
			pre[j++] = in[i];
		}
		else//if cur ele is an operator
		{
			while( !is_stack_empty(&s) && priority(peek_element(&s)) > priority(in[i] ) )
			{
				//pop an ele from the stack and append it into the prefix expression
				pre[j++] = peek_element(&s);
				pop_element(&s);
			}
			//push cur ele onto the stack
			push_element(&s, in[i]);
		}
	}//step3: repeat step1 & step2 till the end of infix expression

	//step4: pop all remaining ele's from the stack one by one and append them into the prefix expression
	while( !is_stack_empty(&s) )
	{
		pre[j++] = peek_element(&s);
		pop_element(&s);
	}
	pre[j] = '\0';
	//step5: reverse the prefix expression
	my_strrev(pre);
}
//for parenthesized as well as non-parenthesized
void infix_to_postfix(char *in, char *post)
{
	stack_t s;
	int i;
	int j=0;

	init_stack(&s);

	//step1: start scanning infix expression from left to right
	for( i = 0 ; in[i] != '\0' ; i++ )
	{
		//step2: if( cur ele is an operand ) -- append it into the postfix expression
		if( is_operand(in[i]))
		{
			post[j++] = in[i];
		}
		else
		if( in[i] == '(' )//if cur ele is an opening brace
		{
			//push it onto the stack
			push_element(&s, in[i]);
		}
		else
		if( in[i] == ')' )//if cur ele is a closing brace
		{
			//pop ele's from the stack till its corresponding opening brace
			//do not ocurres and append them into the postfix expression one by one
			while( peek_element(&s) != '(' )
			{
				post[j++] = peek_element(&s);
				pop_element(&s);
			}
			//pop opening brace from the stack
			pop_element(&s);
		}
		else//if cur ele is an operator
		{
			while( !is_stack_empty(&s) && priority(peek_element(&s)) >= priority(in[i] ) )
			{
				//pop an ele from the stack and append it into the postfix expression
				post[j++] = peek_element(&s);
				pop_element(&s);
			}
			//push cur ele onto the stack
			push_element(&s, in[i]);
		}
	}//step3: repeat step1 & step2 till the end of infix expression

	//step4: pop all remaining ele's from the stack one by one and append them into the postfix expression
	while( !is_stack_empty(&s) )
	{
		post[j++] = peek_element(&s);
		pop_element(&s);
	}
	post[j] = '\0';
}

/*
// only for non-parenthesized infix expression:
void infix_to_postfix(char *in, char *post)
{
	stack_t s;
	int i;
	int j=0;

	init_stack(&s);

	//step1: start scanning infix expression from left to right
	for( i = 0 ; in[i] != '\0' ; i++ )
	{
		//step2: if( cur ele is an operand ) -- append it into the postfix expression
		if( is_operand(in[i]))
		{
			post[j++] = in[i];
		}
		else//if cur ele is an operator
		{
			while( !is_stack_empty(&s) && priority(peek_element(&s)) >= priority(in[i] ) )
			{
				//pop an ele from the stack and append it into the postfix expression
				post[j++] = peek_element(&s);
				pop_element(&s);
			}
			//push cur ele onto the stack
			push_element(&s, in[i]);
		}
	}//step3: repeat step1 & step2 till the end of infix expression

	//step4: pop all remaining ele's from the stack one by one and append them into the postfix expression
	while( !is_stack_empty(&s) )
	{
		post[j++] = peek_element(&s);
		pop_element(&s);
	}
	post[j] = '\0';
}
*/

void init_stack(stack_t *ps)
{
	/*
	int index;
	for( index = 0 ; index < SIZE ; index++ )
		ps->arr[ index ] = 0;
	*/

	//initialize value of top as -1
	ps->top = -1;

}

bool_t is_stack_full(stack_t *ps)
{
	return ( ps->top == SIZE-1 );
}

bool_t is_stack_empty(stack_t *ps)
{
	return ( ps->top == -1 );
}

void push_element(stack_t *ps, int ele)
{
	//step2 - increment the value of top by 1
	ps->top++;
	//step3 - insert/add an ele onto the stack at top position
	ps->arr[ ps->top ] = ele;
	
	//OR
	//ps->arr[ ++ps->top ] = ele;
}

int peek_element(stack_t *ps)
{
	//step2 - return the value of an ele which is at top position
	return ( ps->arr[ ps->top ] );
}

void pop_element(stack_t *ps)
{
	//step2 - decrement the value of top by 1
	ps->top--;
}
