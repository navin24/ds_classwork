//stack applications
#include<stdio.h>
#include<stdlib.h>

#define SIZE 10

typedef int bool_t;

typedef struct
{
	int arr[SIZE];
	int top;
}stack_t;

	
void init_stack(stack_t *ps);
bool_t is_stack_full(stack_t *ps);
bool_t is_stack_empty(stack_t *ps);
void push_element(stack_t *ps, int ele);
int peek_element(stack_t *ps);
void pop_element(stack_t *ps);
void infix_to_postfix(char *in, char *post);
bool_t is_operand(char ch);
int priority(char opr);



int main(void)
{
	char infix[64] = "a*b/c*d/e+f-h";
	char postfix[64] = " ";


	printf("infix expression is: %s\n", infix);
	infix_to_postfix(infix, postfix);
	printf("postfix expression is: %s\n", postfix);

	return 0;
}

bool_t is_operand(char ch)
{
	return ( ( ch >= 65 && ch <= 90 ) || ( ch >= 97 && ch <= 122 ) || ( ch >= 48 && ch <= 57 ) );
}

int priority(char opr)
{
	switch(opr)
	{
		case '*':
		case '/':
		case '$':
		case '%':
		case '^': return 2;

		case '+':
		case '-': return 1;
	}
}

void infix_to_postfix(char *in, char *post)
{
	stack_t s;
	int i;
	int j=0;

	init_stack(&s);

	//step1: start scanning infix expression from left to right
	for( i = 0 ; in[i] != '\0' ; i++ )
	{
		//step2: if( cur ele is an operand ) -- append it into the postfix expression
		if( is_operand(in[i]))
		{
			post[j++] = in[i];
		}
		else//if cur ele is an operator
		{
			while( !is_stack_empty(&s) && priority(peek_element(&s)) >= priority(in[i] ) )
			{
				//pop an ele from the stack and append it into the postfix expression
				post[j++] = peek_element(&s);
				pop_element(&s);
			}
			//push cur ele onto the stack
			push_element(&s, in[i]);
		}
	}//step3: repeat step1 & step2 till the end of infix expression

	//step4: pop all remaining ele's from the stack one by one and append them into the postfix expression
	while( !is_stack_empty(&s) )
	{
		post[j++] = peek_element(&s);
		pop_element(&s);
	}
	post[j] = '\0';
}

void init_stack(stack_t *ps)
{
	/*
	int index;
	for( index = 0 ; index < SIZE ; index++ )
		ps->arr[ index ] = 0;
	*/

	//initialize value of top as -1
	ps->top = -1;

}

bool_t is_stack_full(stack_t *ps)
{
	return ( ps->top == SIZE-1 );
}

bool_t is_stack_empty(stack_t *ps)
{
	return ( ps->top == -1 );
}

void push_element(stack_t *ps, int ele)
{
	//step2 - increment the value of top by 1
	ps->top++;
	//step3 - insert/add an ele onto the stack at top position
	ps->arr[ ps->top ] = ele;
	
	//OR
	//ps->arr[ ++ps->top ] = ele;
}

int peek_element(stack_t *ps)
{
	//step2 - return the value of an ele which is at top position
	return ( ps->arr[ ps->top ] );
}

void pop_element(stack_t *ps)
{
	//step2 - decrement the value of top by 1
	ps->top--;
}
